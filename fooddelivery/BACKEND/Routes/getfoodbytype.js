const express = require("express")

const router = express.Router()

const mongodb = require("mongodb")


const mongoClient = mongodb.MongoClient
 
module.exports = router.get("/:ID", (req, res) => {
    mongoClient.connect("mongodb://localhost:27017/Fooddelivery1", (err, db) => {
        
        if (err){
            res.status(500)
            res.send("Internal server error")
        }
        
        else{
            console.log("DB connected")

            var ID = req.params['ID']
            
            db.collection("Food1").findOne({"ID":ID }, (err, result) => {
                if(err){
                    res.send(err)
                }else{
                    res.status(200).json(result)
                }
            })

            
        }
    })
})