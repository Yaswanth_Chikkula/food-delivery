import React from 'react'
import {NavLink} from 'react-router-dom'


const Header1 = () => {
  return (
    <div>
        <header>
            <NavLink to='/' className='link'>
            Zapp!! MultiCuisine Restaurant
            </NavLink>
            <nav className='navigation'>
                {/* <NavLink to='/about' className='link'>About</NavLink> */}
                <NavLink to='/login' className='link'>LogIn</NavLink>
                <NavLink to='/signup' className='link'>SignUp</NavLink>
            </nav>
        </header>
      
    </div>
  )
}

export default Header1