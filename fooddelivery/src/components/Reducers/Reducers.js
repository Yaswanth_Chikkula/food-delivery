const initialState = {
    login: false
}

const reducer = (state = initialState, action) => {
    switch(action.type){
        case "LOGIN":
            return {
                login: true
            }
        case "LOGOUT":
            return{
                login: false
            }
        default: 
            return state
    }
}

export default reducer