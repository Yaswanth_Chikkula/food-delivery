import React from 'react'

const Footer = () => {
    return (
        <div className='bg-dark text-light p-4'>
            
            <h2>Happy FOODING....</h2>
            <h4>For Any Support-Feel free to Contact Us: 9999999999 (24/7 Helpline Number)</h4>
            <h4 className='text-center'>All rights reserved &copy;</h4>

        </div>

    )
}

export default Footer