import axios from "axios";
import React, { useState } from "react";
const url = "http://127.0.0.1:2700/register";

const SignupForm = () => {
  const [Email, setEmail] = useState("");
  const [Password, setPassword] = useState("");
  const [PhoneNumber, setPhoneNumber] = useState("");
  const [UserName, setUserName] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    try {
      const data = {
      
        Email,
        Password,
        PhoneNumber,
        UserName,
      };
      const response = axios.post(url, data);
      alert("Registration Sucessfull")
      console.log(response.data);
      clearFields();
    } catch (err) {
      console.log(err);
    }
  };
  const clearFields = () => {
    
    setEmail("");
    setPassword("");
    setPhoneNumber("");
    setUserName("");
  };
  return (
    <div className="signup1">
    <div  className="signup-container">
      <h2>Register Now!!</h2>
      <form onSubmit={handleSubmit} className="signup-form">
        <table>
        <tr>
            <td><label htmlFor="UserName">UserName :</label></td>
            <input
            className="inpbox"
              type="text"
              id="UserName"
              name="UserName"
              value={UserName}
              onChange={(event) => {
                setUserName(event.target.value);
              }}
            />
          </tr><br/><br/>
          <tr>
            <td><label htmlFor="Password">Password :</label></td>
            <input
            className="inpbox"
              type="password"
              id="password"
              name="password"
              value={Password}
              onChange={(event) => {
                setPassword(event.target.value);
              }}
            />
          </tr><br/><br/>
          

          <tr>
            <td><label htmlFor="Email">Email : </label></td>
            <input
              className="inpbox"
              type="text"
              id="email"
              name="email"
              value={Email}
              onChange={(event) => {
                setEmail(event.target.value);
              }}
            />
          </tr><br/><br/>
          <tr>
            <td><label htmlFor="PhoneNumber">PhoneNumber :</label></td>
            <input
            className="inpbox"
              type="text"
              id="PhoneNumber"
              name="PhoneNumber"
              value={PhoneNumber}
              onChange={(event) => {
                setPhoneNumber(event.target.value);
              }}
            />
          </tr><br/><br/>
         
          <tr >
            <td colSpan="2"><button type="submit" className="submit-button" ><h2>Register</h2></button></td>
          </tr>
          </table>
      </form>
    </div>
    </div> 
    
  );
};
export default SignupForm;