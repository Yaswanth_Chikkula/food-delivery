import React from 'react';
import { useDispatch } from 'react-redux';

import {NavLink, useNavigate} from 'react-router-dom'


const Header = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const handleLogout = () => {
    dispatch({type: "LOGOUT"})
    return navigate('/login')
  }

  return (
    <div>
      <header className="header">
        <NavLink to='/Foods' className='link'>  <span className='brandlogo'>Zapp!! Multicuisine Restaurant</span></NavLink>
        <nav className='navigation'>
        
        {/* <NavLink to='/AboutUs' className='link'>AboutUs</NavLink>       */}
        <button className='logout-btn'  onClick={handleLogout}>LogOut</button>
      
        </nav>
      </header>
    </div>
  )
}

export default Header