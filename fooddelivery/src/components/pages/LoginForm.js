import React, { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';


function LoginForm() {
    const navigate = useNavigate()
    const [Email, setEmail] = useState('');
    const [Password, setPassword] = useState('');
    // const [user] = useState(false);
    const dispatch = useDispatch()


    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const data = {
                Email,
                Password
            }
            const response = await axios.post('http://localhost:2700/login', data);
            alert("Login Sucessfull")
            dispatch({type: "LOGIN"})
            return navigate('/Foods')

        } catch (error) {
            console.log('Login failed', error);
            alert("Wrong Password")
        }
    };

    return (
        <div className='loginn1'>
        <div className="login-container">
            <h2>Login - Your Food is Waiting...!!</h2><br/><br/>
            <form  onSubmit={handleSubmit} className="login-form">
                <input type="email" className="email" placeholder="Email" value={Email} required onChange={(e) => {setEmail(e.target.value)}} /><br></br><br/>
                <input type="password" className="pass" placeholder="Password" value={Password} required onChange={(e) => {setPassword(e.target.value)}} /><br></br><br></br><br/>
                <button type="submit" className="submit-button"><h2>Login</h2></button>
            </form>
        </div>
        </div>
    );
}

export default LoginForm;