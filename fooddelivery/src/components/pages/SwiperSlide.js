import React from "react"
import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/swiper-bundle.css'
import { Autoplay, EffectCube, Navigation,Pagination,Scrollbar } from 'swiper/modules'

const Swiperslide = () => {
    return (
      <div className="swiperComponent">
        <Swiper
        modules={[Navigation,Pagination,Scrollbar,Autoplay,EffectCube]}
            spaceBetween={50}
            slidesPerView={1}
            navigation
            pagination={{clickable:true}}
            scrollbar={{draggable:true}}
            // slidesPerView={1}
            autoplay={{delay:1000}}
            loop={true}
            speed={3000}
            
        >
            {/* <SwiperSlide>Slide 1</SwiperSlide>
            <SwiperSlide>Slide 2</SwiperSlide>
            <SwiperSlide>Slide 3</SwiperSlide>
            <SwiperSlide>Slide 4</SwiperSlide> */}
           
           <SwiperSlide>
            <img 
            src="https://static.vecteezy.com/system/resources/previews/036/775/538/non_2x/ai-generated-grilled-beef-burger-with-cheese-fries-and-refreshing-cola-generated-by-ai-free-photo.jpg"
            style={{height:"950px"}}
            className="card-img-top" 
            alt="..."
            />
            </SwiperSlide>
            <SwiperSlide>
            <img 
            src="https://ridleysontheriver.co.uk/wp-content/uploads/2020/03/AdobeStock_240675196-scaled.jpeg"
            style={{height:"950px"}}
            className="card-img-top" 
            alt="..."
            />
            </SwiperSlide>
               <SwiperSlide>
            <img 
            src="https://s3-ap-southeast-1.amazonaws.com/assets.limetray.com/assets/user_images/content_images/original/bbk-about-bbk-home.jpg"
            style={{height:"950px"}}
            className="card-img-top" 
            alt="..."
            />
            </SwiperSlide>
               <SwiperSlide>
            <img 
            src="https://upload.wikimedia.org/wikipedia/commons/3/30/Zapp_Scooters_Limited_Company_Logo.png"
            style={{height:"950px"}}
            className="card-img-top" 
            alt="..."
            />
            </SwiperSlide>
        </Swiper>
        </div>
    )
}

export default Swiperslide




