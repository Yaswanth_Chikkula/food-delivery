import React, { useEffect, useState } from "react";
import { Card, CardBody, CardTitle ,CardImg,CardText, Button} from "reactstrap";
import { useNavigate } from 'react-router-dom';

import axios from "axios";
import Swiperslide from "./SwiperSlide";
import Footer from "../Footer";



function Food() {
  const [foods, setfoods] = useState([]);
  const navigate = useNavigate(); 
  const handleBookNow = (id) => {
    localStorage.setItem("id", id)
    navigate("/Fooddetails");
  };

  useEffect(() => {
    try {
      const fetchData = async () => {
        const response = await axios.get("http://localhost:2700/Fetch");
        setfoods(response.data);
      };
      fetchData();
    }catch (err) {
      console.log(err);
     
    };
    
  }, []);



  
  return (
    
    <div>
         <Swiperslide/>
        
         
    {foods.length > 0 ? (
      <div className="card13">
      <div className="cardcontainer">
        {foods.slice(0,3).map((foods, index) => (
          <Card key={index} className="mb-3">
            <CardBody>
              <CardTitle tag="h5">{foods.Name}</CardTitle>
              {foods.image && (
                <CardImg  height="250px" width="400px" src={foods.image} alt={foods.Name} />
              )}
              <CardText>Price : {foods.Price} /-</CardText>
              
              <CardText>Quantity : {foods.Description}</CardText>   
              <Button color="primary" onClick={()=> handleBookNow(foods.ID)}>Let's Eat</Button>           
            </CardBody>
          </Card>
        ))}
      </div>
    
      <div className="cardcontainer">
        {foods.slice(3,6).map((foods, index) => (
          <Card key={index} className="mb-3">
            <CardBody>
              <CardTitle tag="h5">{foods.Name}</CardTitle>
              {foods.image && (
                <CardImg  height="250px" width="400px" src={foods.image} alt={foods.Name} />
              )}
              <CardText>Price : {foods.Price} /-</CardText>
              
              <CardText>Quantity : {foods.Description}</CardText>   
              <Button color="primary" onClick={()=> handleBookNow(foods.ID)}>Let's Eat</Button>           
            </CardBody>
          </Card>
        ))}
      </div>
      <div className="cardcontainer">
        {foods.slice(6,9).map((foods, index) => (
          <Card key={index} className="mb-3">
            <CardBody>
              <CardTitle tag="h5">{foods.Name}</CardTitle>
              {foods.image && (
                <CardImg  height="250px" width="400px" src={foods.image} alt={foods.Name} />
              )}
              <CardText>Price : {foods.Price} /-</CardText>
              
              <CardText>Quantity : {foods.Description}</CardText>   
              <Button color="primary" onClick={()=> handleBookNow(foods.ID)}>Let's Eat</Button>           
            </CardBody>
          </Card>
        ))}
      </div>
      <div className="cardcontainer">
        {foods.slice(9,12).map((foods, index) => (
          <Card key={index} className="mb-3">
            <CardBody>
              <CardTitle tag="h5">{foods.Name}</CardTitle>
              {foods.image && (
                <CardImg  height="250px" width="400px" src={foods.image} alt={foods.Name} />
              )}
              <CardText>Price : {foods.Price} /-</CardText>
              
              <CardText>Quantity : {foods.Description}</CardText>   
              <Button color="primary" onClick={()=> handleBookNow(foods.ID)}>Let's Eat</Button>           
            </CardBody>
          </Card>
        ))}
      </div >
      <div className="cardcontainer">
        {foods.slice(12,15).map((foods, index) => (
          <Card key={index} className="mb-3">
            <CardBody>
              <CardTitle tag="h5">{foods.Name}</CardTitle>
              {foods.image && (
                <CardImg  height="250px" width="400px" src={foods.image} alt={foods.Name} />
              )}
              <CardText>Price : {foods.Price} /-</CardText>
              
              <CardText>Quantity : {foods.Description}</CardText>   
              <Button color="primary" onClick={()=> handleBookNow(foods.ID)}>Let's Eat</Button>           
            </CardBody>
          </Card>
        ))}
      </div>
      
      
      <Footer/>
    </div>
      
    ) : (
      <p>No Foods found</p>
    )}
  </div>
  );
}
export default Food;