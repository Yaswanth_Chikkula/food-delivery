import {createStore} from "redux"
import reducer from "./components/Reducers/Reducers"
import { composeWithDevTools } from 'redux-devtools-extension'
export const store = createStore(reducer, composeWithDevTools())