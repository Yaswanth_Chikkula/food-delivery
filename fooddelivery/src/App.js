
import './App.css';
import Homepage from './components/pages/Homepage';
import Footer from './components/Footer';





import Header from './components/pages/Header';
// import Login from './components/pages/Login';
// import Register from './components/pages/Register';
import Swiperslide from './components/pages/SwiperSlide';
import LoginForm from './components/pages/LoginForm';
import { Route, Routes } from 'react-router-dom';
import SignupForm from './components/pages/Signup';
import Food from './components/pages/Food';
import { useSelector } from 'react-redux';
import Header1 from './components/Navbar';
import Fooddetails from './components/pages/Fooddetails';






function App() {
  const login = useSelector((state)=>state.login)
  console.log(login);
  return (
    
    <div className="App">


     {login ? <Header/> : <Header1/>} 

      
      <Routes>
        {/* <Route path='/' element = {<Homepage />}></Route> */}
      <Route path = "/login" element = {<LoginForm />}></Route>
      <Route path = "/signup" element = {<SignupForm />}></Route>
      <Route path = "/Foods" element = {<Food />}></Route>
      {/* <Route path = "/Fooddetails" element = {<Fooddetails />}></Route> */}
    
      </Routes>
      {/* <Footer/> */}

      
    </div>
 
  );
}


export default App;
